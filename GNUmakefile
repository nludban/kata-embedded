##
## kata-embedded/GNUmakefile
##

all:	libcheck tests

CHECK_TGZ ?= $(HOME)/Download/check-0.10.0.tar.gz

CP	= /bin/cp
MKDIR	= /bin/mkdir -p
RM	= /bin/rm -f

CC := $(shell { command -v clang || command -v gcc; } 2>/dev/null)

CFLAGS	= -Wall -g
#CFLAGS	+= -O
CFLAGS	+= -Ibuild/include
CFLAGS	+= -Ibuild/libcheck/include
ifeq ($(CC), /usr/bin/clang)
CFLAGS	+= -fno-color-diagnostics
endif

LDFLAGS	= -Lbuild/libcheck/lib

#---------------------------------------------------------------------#

build/libcheck: $(CHECK_TGZ)
	(cd build && tar -zxvf $(CHECK_TGZ))
	(cd build/check-0.10.0/ && ./configure	\
		CC=$(CC) CFLAGS="$(CFLAGS)"	\
		--prefix=$(PWD)/build/libcheck)
	(cd build/check-0.10.0 && $(MAKE))
	(cd build/check-0.10.0 && $(MAKE) install)

#(cd build/check-0.10.0 && $(MAKE) check)

libcheck: build/libcheck

#---------------------------------------------------------------------#

build/include/liblist/list.h:			\
		liblist/include/liblist/list.h
	@$(MKDIR) build/include/liblist
	$(CP) liblist/include/liblist/list.h	\
		build/include/liblist/list.h


build/obj/list.o:				\
		build/include/liblist/list.h	\
		liblist/src/list.c
	@$(MKDIR) build/obj
	$(CC) $(CFLAGS)				\
		-o build/obj/list.o		\
		-c liblist/src/list.c

build/obj/testlist.o:				\
		build/include/liblist/list.h	\
		liblist/test/testlist.c
	@$(MKDIR) build/obj
	$(CC) $(CFLAGS)				\
		-o build/obj/testlist.o		\
		-c liblist/test/testlist.c

build/test/testlist:				\
		build/obj/list.o		\
		build/obj/testlist.o
	@$(MKDIR) build/test
	$(CC) $(LDFLAGS)			\
		-o build/test/testlist		\
		build/obj/list.o		\
		build/obj/testlist.o		\
		-lcheck

#---------------------------------------------------------------------#

build/include/libroman/roman.h:			\
		libroman/include/libroman/roman.h
	@$(MKDIR) build/include/libroman
	$(CP) libroman/include/libroman/roman.h	\
		build/include/libroman/roman.h

build/include/libroman/calculator.h:		\
		libroman/include/libroman/calculator.h
	@$(MKDIR) build/include/libroman
	$(CP) libroman/include/libroman/calculator.h \
		build/include/libroman/calculator.h


build/obj/roman.o:				\
		build/include/libroman/roman.h	\
		libroman/src/roman.c
	@$(MKDIR) build/obj
	$(CC) $(CFLAGS)				\
		-o build/obj/roman.o		\
		-c libroman/src/roman.c

build/obj/calculator.o:				\
		build/include/libroman/roman.h	\
		build/include/libroman/calculator.h \
		libroman/src/calculator.c
	@$(MKDIR) build/obj
	$(CC) $(CFLAGS)				\
		-o build/obj/calculator.o	\
		-c libroman/src/calculator.c


build/obj/testroman.o:				\
		build/include/libroman/roman.h	\
		libroman/test/testroman.c
	@$(MKDIR) build/obj
	$(CC) $(CFLAGS)				\
		-o build/obj/testroman.o	\
		-c libroman/test/testroman.c

build/test/testroman:				\
		build/obj/roman.o		\
		build/obj/list.o		\
		build/obj/testroman.o
	@$(MKDIR) build/test
	$(CC) $(LDFLAGS)			\
		-o build/test/testroman		\
		build/obj/roman.o		\
		build/obj/list.o		\
		build/obj/testroman.o		\
		-lcheck


build/obj/testcalculator.o:			\
		build/include/libroman/calculator.h \
		libroman/test/testcalculator.c
	@$(MKDIR) build/obj
	$(CC) $(CFLAGS)				\
		-o build/obj/testcalculator.o	\
		-c libroman/test/testcalculator.c

build/test/testcalculator:			\
		build/obj/calculator.o		\
		build/obj/roman.o		\
		build/obj/list.o		\
		build/obj/testcalculator.o
	@$(MKDIR) build/test
	$(CC) $(LDFLAGS)			\
		-o build/test/testcalculator	\
		build/obj/calculator.o		\
		build/obj/roman.o		\
		build/obj/list.o		\
		build/obj/testcalculator.o	\
		-lcheck

#---------------------------------------------------------------------#

.PHONY: testlist testroman testcalculator tests

testlist:					\
		build/test/testlist
	./build/test/testlist

testroman:					\
		build/test/testroman
	./build/test/testroman

testcalculator:					\
		build/test/testcalculator
	./build/test/testcalculator

tests:						\
	testlist				\
	testroman				\
	testcalculator

#---------------------------------------------------------------------#

.PHONY: clean distclean

clean:
	$(RM) build/obj/*

distclean:
	$(RM) -r build/*

#--#
