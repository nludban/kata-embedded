/*********************************************************************/
/*
 * <liblist/list.h>
 */
/*********************************************************************/

#ifndef _LIBLIST__LIST_H_
#define _LIBLIST__LIST_H_

/*********************************************************************/

struct list
{
   const char		*value;
   struct list		*next;
};

typedef struct list	*list_t;

list_t list_cons(const char *value, list_t next);
const char * list_car(list_t);
list_t list_cdr(list_t);
void list_free(list_t);

/*********************************************************************/
#endif

