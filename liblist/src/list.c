/*********************************************************************/
/*
 * liblist/src/list.c
 */
/*********************************************************************/

#include <liblist/list.h>

#include <stdlib.h>

/*********************************************************************/

list_t
list_cons(
   const char		*value,
   list_t		next
   )
{
   list_t l = (struct list *)malloc(sizeof(struct list));
   l->value = value;
   l->next = next;
   return l;
}

const char *
list_car(
   list_t		l
   )
{
   return l->value;
}

list_t
list_cdr(
   list_t		l
   )
{
   return l->next;
}

void
list_free(
   list_t		l
   )
{
   if (l != NULL) {
      list_free(list_cdr(l));
      free(l);
   }
   return;
}

/**/
