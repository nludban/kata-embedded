/*********************************************************************/
/*
 * liblist/test/testlist.c
 */
/*********************************************************************/

#include <liblist/list.h>

#include <check.h>

/*********************************************************************/

START_TEST(test_short_list)
{
   list_t x = NULL;

   x = list_cons("A", x);

   ck_assert(x != NULL);

   ck_assert_str_eq(list_car(x), "A");

   ck_assert(list_cdr(x) == NULL);

   list_free(x);
}
END_TEST


START_TEST(test_long_list)
{
   list_t x = NULL;

   x = list_cons("A", x);
   ck_assert(x != NULL);

   x = list_cons("B", x);
   ck_assert(x != NULL);

   x = list_cons("C", x);
   ck_assert(x != NULL);

   ck_assert(list_cdr(x) != NULL);
   ck_assert(list_cdr(list_cdr(x)) != NULL);
   ck_assert(list_cdr(list_cdr(list_cdr(x))) == NULL);

   ck_assert_str_eq(list_car(x), "C");
   ck_assert_str_eq(list_car(list_cdr(x)), "B");
   ck_assert_str_eq(list_car(list_cdr(list_cdr(x))), "A");

   list_free(x);
}
END_TEST


Suite *
list_suite()
{
   Suite *s = suite_create("List");

   TCase *tc = tcase_create("Core");
   tcase_add_test(tc, test_short_list);
   tcase_add_test(tc, test_long_list);

   suite_add_tcase(s, tc);

   return s;
}

/*********************************************************************/

int
main(
   int			argc,
   char			**argv
   )
{
   Suite *s = list_suite();

   SRunner *sr = srunner_create(s);

   srunner_run_all(sr, CK_NORMAL);
   int nfail = srunner_ntests_failed(sr);
   srunner_free(sr);

   return (nfail == 0 ? 0 : 1);
}

/**/
