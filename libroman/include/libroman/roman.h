/*********************************************************************/
/*
 * <libroman/roman.h>
 */
/*********************************************************************/

#ifndef _LIBROMAN__ROMAN_H_
#define _LIBROMAN__ROMAN_H_

/*********************************************************************/

struct roman;
typedef struct roman *roman_t;

roman_t roman_parse(const char *);

int roman_format(roman_t, char *buf, char *end);

void roman_free(roman_t);


int roman_add_inplace(roman_t a, const roman_t b);

int roman_sub_inplace(roman_t a, const roman_t b);

/*********************************************************************/
#endif
