/*********************************************************************/
/*
 * <libroman/calculator.h>
 */
/*********************************************************************/

#ifndef _LIBROMAN__CALCULATOR_H_
#define _LIBROMAN__CALCULATOR_H_

#include <stdlib.h>

/*********************************************************************/

// 0 = no error
// EINVAL = bad format (input)
// ERANGE = output buffer too small, or negative result

int roman_add(const char *a, const char *b,
	      char *result, size_t result_size);

int roman_sub(const char *a, const char *b,
	      char *result, size_t result_size);

/*********************************************************************/
#endif
