/*********************************************************************/
/*
 * libroman/src/roman.c
 */
/*********************************************************************/

#include <libroman/roman.h>

#include <liblist/list.h>

#include <errno.h>
#include <stdlib.h>
#include <string.h>


struct roman
{
   list_t		m;
   list_t		c;
   list_t		x;
   list_t		i;
};

/*********************************************************************/

static
roman_t
roman_alloc()
{
   roman_t r = (struct roman *)malloc(sizeof(struct roman));
   r->m = NULL;
   r->c = NULL;
   r->x = NULL;
   r->i = NULL;
   return r;
}

void
roman_free(
   roman_t		r
   )
{
   if (r != NULL) {
      list_free(r->m);
      r->c = NULL;
      r->x = NULL;
      r->i = NULL;
      free(r);
   }
   return;
}

/*********************************************************************/

#define ROMAN_DIGITS(X, V, I)					\
   list_cons("",						\
	list_cons(I,						\
	     list_cons(I I,					\
		  list_cons(I I I,				\
		       list_cons(I V,				\
			    list_cons(V,			\
				 list_cons(V I,			\
				      list_cons(V I I,		\
					   list_cons(V I I I,	\
						list_cons(I X, NULL))))))))))

static list_t		c_digits = NULL;
static list_t		x_digits = NULL;
static struct list	*i_digits;

static
void
digits_init()
{
   if (c_digits != NULL)
      return;

   c_digits = ROMAN_DIGITS("M", "D", "C");
   x_digits = ROMAN_DIGITS("C", "L", "X");
   i_digits = ROMAN_DIGITS("X", "V", "I");
   return;
}


/*********************************************************************/

static
struct list *
digits_parse_m(
   const char		**s
   )
{
   struct list *m = NULL;

   while (**s == 'M') {
      m = list_cons("M", m);
      (*s)++;
   }

   return m;
}

static
list_t
digits_parse(
   list_t		digits,
   const char		**s
   )
{
   list_t m = digits;
   for (digits = list_cdr(digits);
	digits != NULL;
	digits = list_cdr(digits))
      if (!strncmp(list_car(digits), *s, strlen(list_car(digits))))
	 m = digits;
   *s += strlen(list_car(m));
   return m;
}

roman_t
roman_parse(
   const char		*s
   )
{
   struct roman *r = roman_alloc();

   r->m = digits_parse_m(&s);

   digits_init();

   r->c = digits_parse(c_digits, &s);
   r->x = digits_parse(x_digits, &s);
   r->i = digits_parse(i_digits, &s);

   if (*s == '\0')
      return (r);

   roman_free(r);
   return (NULL);
}


static
char *
string_append(
   char			*dst,
   char			*end,
   const char		*src
   )
{
   while (dst < end) {
      *dst = *src;
      if (*dst == '\0')
	 return (dst);
      dst++;
      src++;
   }
   return (NULL);
}


int
roman_format(
   roman_t		r,
   char			*s,
   char			*end
   )
{
   *s = '\0';

   list_t m = r->m;
   while (m != NULL) {
      s = string_append(s, end, "M");
      if (s == NULL)
	 return (ERANGE);
      m = list_cdr(m);
   }

   s = string_append(s, end, list_car(r->c));
   if (s == NULL)
      return (ERANGE);

   s = string_append(s, end, list_car(r->x));
   if (s == NULL)
      return (ERANGE);

   s = string_append(s, end, list_car(r->i));
   if (s == NULL)
      return (ERANGE);

   return (0);
}

/*********************************************************************/

static void
roman_add_m(
   roman_t		a
   )
{
   a->m = list_cons("M", a->m);
   return;
}

static void
roman_add_c(
   roman_t		a
   )
{
   a->c = list_cdr(a->c);
   if (a->c == NULL) {
      a->c = c_digits;
      roman_add_m(a);
   }
   return;
}

static void
roman_add_x(
   roman_t		a
   )
{
   a->x = list_cdr(a->x);
   if (a->x == NULL) {
      a->x = x_digits;
      roman_add_c(a);
   }
   return;
}

static void
roman_add_i(
   roman_t		a
   )
{
   a->i = list_cdr(a->i);
   if (a->i == NULL) {
      a->i = i_digits;
      roman_add_x(a);
   }
   return;
}

int
roman_add_inplace(
   roman_t		a,
   const roman_t	b
   )
{
   struct list *m = b->m;
   while (m != NULL) {
      roman_add_m(a);
      m = list_cdr(m);
   }

   struct list *c = c_digits;
   while (list_car(c) != list_car(b->c)) {
      roman_add_c(a);
      c = list_cdr(c);
   }

   struct list *x = x_digits;
   while (list_car(x) != list_car(b->x)) {
      roman_add_x(a);
      x = list_cdr(x);
   }

   struct list *i = i_digits;
   while (list_car(i) != list_car(b->i)) {
      roman_add_i(a);
      i = list_cdr(i);
   }

   return (0);
}

/*********************************************************************/

static
int
roman_sub_m(
   roman_t		a
   )
{
   list_t h = a->m;
   if (h == NULL)
      return (-1);
   a->m = list_cdr(h);
   free(h);
   return (0);
}

static
int
roman_sub_c(
   roman_t		a
   )
{
   list_t p = c_digits;
   while ((list_cdr(p) != a->c) && (list_cdr(p) != NULL))
      p = list_cdr(p);
   if ((list_cdr(p) == NULL) && (roman_sub_m(a) != 0))
      return (-1);
   a->c = p;
   return (0);
}

static
int
roman_sub_x(
   roman_t		a
   )
{
   list_t p = x_digits;
   while ((list_cdr(p) != a->x) && (list_cdr(p) != NULL))
      p = list_cdr(p);
   if ((list_cdr(p) == NULL) && (roman_sub_c(a) != 0))
      return (ERANGE);
   a->x = p;
   return (0);
}

static
int
roman_sub_i(
   roman_t		a
   )
{
   list_t p = i_digits;
   while ((list_cdr(p) != a->i) && (list_cdr(p) != NULL))
      p = list_cdr(p);
   if ((list_cdr(p) == NULL) && (roman_sub_x(a) != 0))
      return (ERANGE);
   a->i = p;
   return (0);
}

int
roman_sub_inplace(
   roman_t		a,
   const roman_t	b
   )
{
   list_t m = b->m;
   while (m != NULL) {
      if (roman_sub_m(a) != 0)
	 return (ERANGE);
      m = list_cdr(m);
   }

   list_t c = c_digits;
   while (list_car(c) != list_car(b->c)) {
      if (roman_sub_c(a) != 0)
	 return (ERANGE);
      c = list_cdr(c);
   }

   list_t x = x_digits;
   while (list_car(x) != list_car(b->x)) {
      if (roman_sub_x(a) != 0)
	 return (ERANGE);
      x = list_cdr(x);
   }

   list_t i = i_digits;
   while (list_car(i) != list_car(b->i)) {
      if (roman_sub_i(a) != 0)
	 return (ERANGE);
      i = list_cdr(i);
   }

   return (0);
}

/**/
