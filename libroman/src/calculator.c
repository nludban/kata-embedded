/*********************************************************************/
/*
 * libroman/src/calculator.c
 */
/*********************************************************************/

#include <libroman/calculator.h>

#include <libroman/roman.h>

#include <errno.h>

/*********************************************************************/

int
roman_add(
   const char		*a,
   const char		*b,
   char			*result,
   size_t		result_size
   )
{
   roman_t ra = roman_parse(a);
   if (ra == NULL)
      return (EINVAL);

   roman_t rb = roman_parse(b);
   if (rb == NULL) {
      roman_free(ra);
      return (EINVAL);
   }

   int err = roman_add_inplace(ra, rb);
   if (err == 0)
      err = roman_format(ra, result, result+result_size);

   roman_free(ra);
   roman_free(rb);
   return (err == 0 ? 0 : ERANGE);
}


int
roman_sub(
   const char		*a,
   const char		*b,
   char			*result,
   size_t		result_size
   )
{
   roman_t ra = roman_parse(a);
   if (ra == NULL)
      return (EINVAL);

   roman_t rb = roman_parse(b);
   if (rb == NULL) {
      roman_free(ra);
      return (EINVAL);
   }

   int err = roman_sub_inplace(ra, rb);
   if (err == 0)
      err = roman_format(ra, result, result+result_size);

   roman_free(ra);
   roman_free(rb);
   return (err == 0 ? 0 : ERANGE);
}


/**/
