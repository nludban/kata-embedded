/*********************************************************************/
/*
 * libroman/test/testroman.c
 */
/*********************************************************************/

#include <libroman/roman.h>

#include <check.h>

#include <errno.h>

/*********************************************************************/

START_TEST(test_roman_parse_m)
{
   roman_t r;

   r = roman_parse("");
   ck_assert(r != NULL);
   roman_free(r);

   r = roman_parse("junk");
   ck_assert(r == NULL);

   r = roman_parse("M");
   ck_assert(r != NULL);
   roman_free(r);

   r = roman_parse("MMM");
   ck_assert(r != NULL);
   roman_free(r);


}
END_TEST


START_TEST(test_roman_format_m)
{
   char			buf[100];
   roman_t		r;
   int			err;

   r = roman_parse("");
   err = roman_format(r, buf, buf+10);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "");
   roman_free(r);

   r = roman_parse("M");
   err = roman_format(r, buf, buf+10);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "M");
   roman_free(r);

   r = roman_parse("MMM");
   err = roman_format(r, buf, buf+4);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "MMM");
   roman_free(r);

   r = roman_parse("MMMMM");
   err = roman_format(r, buf, buf+5);
   ck_assert(err == ERANGE);
   roman_free(r);

}
END_TEST


START_TEST(test_roman_add_m)
{
   char			buf[100];
   roman_t		a;
   roman_t		b;

   a = roman_parse("");
   b = roman_parse("");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "");
   roman_free(a);
   roman_free(b);

   a = roman_parse("M");
   b = roman_parse("");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "M");
   roman_free(a);
   roman_free(b);

   a = roman_parse("");
   b = roman_parse("M");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "M");
   roman_free(a);
   roman_free(b);

   a = roman_parse("M");
   b = roman_parse("M");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "MM");
   roman_free(a);
   roman_free(b);

   a = roman_parse("MM");
   b = roman_parse("MMM");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "MMMMM");
   roman_free(a);
   roman_free(b);

}
END_TEST


START_TEST(test_roman_parse_mc)
{
   roman_t r;

   r = roman_parse("C");
   ck_assert(r != NULL);
   roman_free(r);

   r = roman_parse("CCC");
   ck_assert(r != NULL);
   roman_free(r);

   r = roman_parse("D");
   ck_assert(r != NULL);
   roman_free(r);

   r = roman_parse("CD");
   ck_assert(r != NULL);
   roman_free(r);

   r = roman_parse("DC");
   ck_assert(r != NULL);
   roman_free(r);

   r = roman_parse("DCCC");
   ck_assert(r != NULL);
   roman_free(r);

   r = roman_parse("CM");
   ck_assert(r != NULL);
   roman_free(r);


}
END_TEST


START_TEST(test_roman_format_mc)
{
   char			buf[100];
   roman_t		r;
   int			err;

   r = roman_parse("C");
   err = roman_format(r, buf, buf+10);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "C");
   roman_free(r);

   r = roman_parse("CCC");
   err = roman_format(r, buf, buf+4);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "CCC");
   roman_free(r);

   r = roman_parse("D");
   err = roman_format(r, buf, buf+4);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "D");
   roman_free(r);

   r = roman_parse("CD");
   err = roman_format(r, buf, buf+4);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "CD");
   roman_free(r);

   r = roman_parse("DC");
   err = roman_format(r, buf, buf+4);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "DC");
   roman_free(r);

   r = roman_parse("DCCC");
   err = roman_format(r, buf, buf+10);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "DCCC");
   roman_free(r);

   r = roman_parse("CM");
   err = roman_format(r, buf, buf+10);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "CM");
   roman_free(r);


}
END_TEST


START_TEST(test_roman_add_mc)
{
   char			buf[100];
   roman_t		a;
   roman_t		b;

   a = roman_parse("C");
   b = roman_parse("");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "C");
   roman_free(a);
   roman_free(b);

   a = roman_parse("");
   b = roman_parse("C");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "C");
   roman_free(a);
   roman_free(b);

   a = roman_parse("C");
   b = roman_parse("C");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "CC");
   roman_free(a);
   roman_free(b);

   a = roman_parse("CC");
   b = roman_parse("CC");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "CD");
   roman_free(a);
   roman_free(b);

   a = roman_parse("CCC");
   b = roman_parse("CCC");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "DC");
   roman_free(a);
   roman_free(b);

   a = roman_parse("CD");
   b = roman_parse("DC");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "M");
   roman_free(a);
   roman_free(b);

}
END_TEST


START_TEST(test_roman_parse_cx)
{
   roman_t r;

   r = roman_parse("X");
   ck_assert(r != NULL);
   roman_free(r);

   r = roman_parse("XXX");
   ck_assert(r != NULL);
   roman_free(r);

   r = roman_parse("L");
   ck_assert(r != NULL);
   roman_free(r);

   r = roman_parse("XL");
   ck_assert(r != NULL);
   roman_free(r);

   r = roman_parse("LX");
   ck_assert(r != NULL);
   roman_free(r);

   r = roman_parse("LXXX");
   ck_assert(r != NULL);
   roman_free(r);

   r = roman_parse("XC");
   ck_assert(r != NULL);
   roman_free(r);


}
END_TEST


START_TEST(test_roman_format_cx)
{
   char			buf[100];
   roman_t		r;
   int			err;

   r = roman_parse("X");
   err = roman_format(r, buf, buf+10);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "X");
   roman_free(r);

   r = roman_parse("XXX");
   err = roman_format(r, buf, buf+4);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "XXX");
   roman_free(r);

   r = roman_parse("L");
   err = roman_format(r, buf, buf+4);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "L");
   roman_free(r);

   r = roman_parse("XL");
   err = roman_format(r, buf, buf+4);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "XL");
   roman_free(r);

   r = roman_parse("LX");
   err = roman_format(r, buf, buf+4);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "LX");
   roman_free(r);

   r = roman_parse("LXXX");
   err = roman_format(r, buf, buf+10);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "LXXX");
   roman_free(r);

   r = roman_parse("XC");
   err = roman_format(r, buf, buf+10);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "XC");
   roman_free(r);


}
END_TEST


START_TEST(test_roman_add_cx)
{
   char			buf[100];
   roman_t		a;
   roman_t		b;

   a = roman_parse("X");
   b = roman_parse("");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "X");
   roman_free(a);
   roman_free(b);

   a = roman_parse("");
   b = roman_parse("X");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "X");
   roman_free(a);
   roman_free(b);

   a = roman_parse("X");
   b = roman_parse("X");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "XX");
   roman_free(a);
   roman_free(b);

   a = roman_parse("XX");
   b = roman_parse("XX");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "XL");
   roman_free(a);
   roman_free(b);

   a = roman_parse("XXX");
   b = roman_parse("XXX");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "LX");
   roman_free(a);
   roman_free(b);

   a = roman_parse("XL");
   b = roman_parse("LX");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "C");
   roman_free(a);
   roman_free(b);

}
END_TEST


START_TEST(test_roman_parse_xi)
{
   roman_t r;

   r = roman_parse("I");
   ck_assert(r != NULL);
   roman_free(r);

   r = roman_parse("III");
   ck_assert(r != NULL);
   roman_free(r);

   r = roman_parse("V");
   ck_assert(r != NULL);
   roman_free(r);

   r = roman_parse("IV");
   ck_assert(r != NULL);
   roman_free(r);

   r = roman_parse("VI");
   ck_assert(r != NULL);
   roman_free(r);

   r = roman_parse("VIII");
   ck_assert(r != NULL);
   roman_free(r);

   r = roman_parse("IX");
   ck_assert(r != NULL);
   roman_free(r);


}
END_TEST


START_TEST(test_roman_format_xi)
{
   char			buf[100];
   roman_t		r;
   int			err;

   r = roman_parse("I");
   err = roman_format(r, buf, buf+10);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "I");
   roman_free(r);

   r = roman_parse("III");
   err = roman_format(r, buf, buf+4);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "III");
   roman_free(r);

   r = roman_parse("V");
   err = roman_format(r, buf, buf+4);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "V");
   roman_free(r);

   r = roman_parse("IV");
   err = roman_format(r, buf, buf+4);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "IV");
   roman_free(r);

   r = roman_parse("VI");
   err = roman_format(r, buf, buf+4);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "VI");
   roman_free(r);

   r = roman_parse("VIII");
   err = roman_format(r, buf, buf+10);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "VIII");
   roman_free(r);

   r = roman_parse("IX");
   err = roman_format(r, buf, buf+10);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "IX");
   roman_free(r);


}
END_TEST


START_TEST(test_roman_add_xi)
{
   char			buf[100];
   roman_t		a;
   roman_t		b;

   a = roman_parse("I");
   b = roman_parse("");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "I");
   roman_free(a);
   roman_free(b);

   a = roman_parse("");
   b = roman_parse("I");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "I");
   roman_free(a);
   roman_free(b);

   a = roman_parse("I");
   b = roman_parse("I");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "II");
   roman_free(a);
   roman_free(b);

   a = roman_parse("II");
   b = roman_parse("II");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "IV");
   roman_free(a);
   roman_free(b);

   a = roman_parse("III");
   b = roman_parse("III");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "VI");
   roman_free(a);
   roman_free(b);

   a = roman_parse("IV");
   b = roman_parse("VI");
   ck_assert(roman_add_inplace(a, b) == 0);
   ck_assert(roman_format(a, buf, buf+10) == 0);
   ck_assert_str_eq(buf, "X");
   roman_free(a);
   roman_free(b);

}
END_TEST


Suite *
roman_suite()
{
   Suite *s = suite_create("Roman");

   TCase *tc = tcase_create("Core");
   tcase_add_test(tc, test_roman_parse_m);
   tcase_add_test(tc, test_roman_format_m);
   tcase_add_test(tc, test_roman_add_m);

   tcase_add_test(tc, test_roman_parse_mc);
   tcase_add_test(tc, test_roman_format_mc);
   tcase_add_test(tc, test_roman_add_mc);

   tcase_add_test(tc, test_roman_parse_cx);
   tcase_add_test(tc, test_roman_format_cx);
   tcase_add_test(tc, test_roman_add_cx);

   tcase_add_test(tc, test_roman_parse_xi);
   tcase_add_test(tc, test_roman_format_xi);
   tcase_add_test(tc, test_roman_add_xi);

   suite_add_tcase(s, tc);

   return s;
}

/*********************************************************************/

int
main(
   int			argc,
   char			**argv
   )
{
   Suite *s = roman_suite();

   SRunner *sr = srunner_create(s);

   srunner_run_all(sr, CK_NORMAL);
   int nfail = srunner_ntests_failed(sr);
   srunner_free(sr);

   return (nfail == 0 ? 0 : 1);
}

/**/
