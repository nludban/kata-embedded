/*********************************************************************/
/*
 * libroman/test/testcalculator.c
 */
/*********************************************************************/

#include <libroman/calculator.h>

#include <check.h>

#include <errno.h>

/*********************************************************************/

START_TEST(test_calculator_add)
{
   char			buf[100];
   int			err;

   err = roman_add("", "", buf, 100);
   ck_assert(err == 0);

   err = roman_add("junk", "", buf, 100);
   ck_assert(err == EINVAL);

   err = roman_add("", "junk", buf, 100);
   ck_assert(err == EINVAL);

   err = roman_add("MM", "MMM", buf, 6);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "MMMMM");

   err = roman_add("MM", "MMM", buf, 5);
   ck_assert(err == ERANGE);

}
END_TEST


START_TEST(test_examples_addition)
{
   char			buf[100];
   int			err;

   err = roman_add("XIV", "LX", buf, 100);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "LXXIV");

   err = roman_add("XX", "II", buf, 100);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "XXII");

   err = roman_add("II", "II", buf, 100);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "IV");

   err = roman_add("XX", "XX", buf, 100);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "XL");

   err = roman_add("CC", "CC", buf, 100);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "CD");

   err = roman_add("V", "V", buf, 100);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "X");

   err = roman_add("L", "L", buf, 100);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "C");

   err = roman_add("D", "D", buf, 100);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "M");

}
END_TEST


START_TEST(test_examples_subtraction)
{
   char			buf[100];
   int			err;

   err = roman_sub("LXXIV", "LX", buf, 100);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "XIV");

   err = roman_sub("LXXIV", "LX", buf, 100);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "XIV");


   err = roman_sub("XXII", "II", buf, 100);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "XX");

   err = roman_sub("XXII", "XX", buf, 100);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "II");


   err = roman_sub("IV", "II", buf, 100);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "II");


   err = roman_sub("XL", "XX", buf, 100);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "XX");


   err = roman_sub("CD", "CC", buf, 100);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "CC");


   err = roman_sub("X", "V", buf, 100);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "V");


   err = roman_sub("C", "L", buf, 100);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "L");

   err = roman_sub("M", "D", buf, 100);
   ck_assert(err == 0);
   ck_assert_str_eq(buf, "D");

}
END_TEST


Suite *
calculator_suite()
{
   Suite *s = suite_create("Calculator");

   TCase *tc = tcase_create("Core");
   tcase_add_test(tc, test_calculator_add);
   tcase_add_test(tc, test_examples_addition);
   tcase_add_test(tc, test_examples_subtraction);

   suite_add_tcase(s, tc);

   return s;
}

/*********************************************************************/

int
main(
   int			argc,
   char			**argv
   )
{
   Suite *s = calculator_suite();

   SRunner *sr = srunner_create(s);

   srunner_run_all(sr, CK_NORMAL);
   int nfail = srunner_ntests_failed(sr);
   srunner_free(sr);

   return (nfail == 0 ? 0 : 1);
}

/**/
