/*********************************************************************/
/*
 * romancalc/src/romancalc.c
 */
/*********************************************************************/

#include <libroman/roman.h>

#include <errno.h>
#include <stdio.h>
#include <string.h>

int
main(
   int			argc,
   char			**argv
   )
{
   if (argc < 2) {
      // Usage:
      return (1);
   }

   roman_t a = roman_parse(argv[1]);
   if (a == NULL) {
      fprintf(stderr, "Invalid arg[1]: '%s'.\n", argv[1]);
      return (1);
   }

   printf("  ");
   int k = 2;
   for (;;) {

      char abuf[100];
      if (roman_format(a, abuf, abuf+sizeof(abuf)) != 0) {
	 fprintf(stderr, "String too long.\n");
	 return (1);
      }
      printf("%s\n", abuf);

      if (k == argc)
	 break;

      int op = 0;
      if (!strcmp(argv[k], "+"))
	 op = '+';
      if (!strcmp(argv[k], "-"))
	 op = '-';
      if (op == 0) {
	 fprintf(stderr, "Invalid operation '%s'.\n", argv[k]);
	 return (1);
      }

      k++;
      if (k == argc) {
	 fprintf(stderr, "Missing operand.\n");
	 return (1);
      }

      roman_t b = roman_parse(argv[k]);
      if (b == NULL) {
	 fprintf(stderr, "Invalid arg[%i]: '%s'.\n", k, argv[k]);
	 return (1);
      }

      k++;

      char bbuf[100];
      if (roman_format(b, bbuf, bbuf+sizeof(bbuf)) != 0) {
	 fprintf(stderr, "String too long.\n");
	 return (1);
      }
      printf("%c %s\n", op, bbuf);

      switch (op) {
      case '+':
	 roman_add_inplace(a, b);
	 break;
      case '-':
	 if (roman_sub_inplace(a, b) != 0)
	    printf("<underflow> ");
	 break;
      }

      roman_free(b);

      printf("= ");

   }

   roman_free(a);
   return (0);
}

/**/
